#include <drogon/drogon.h>

int main() {
    // Set HTTP listener address and port
    static constexpr uint16_t port = 60001;
    drogon::app().addListener("127.0.0.1", port);
    // Load config file
    drogon::app().loadConfigFile("../config.json");
    // Run HTTP framework,the method will block in the internal event loop
    drogon::app().run();
    return 0;
}
