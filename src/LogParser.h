#pragma once
#include <string>
#include <utility>

class LogParser {
    explicit LogParser(std::string log_path) : log_path_(std::move(log_path)) {}
    void read_to_db();

private:
    std::string log_path_;
};
