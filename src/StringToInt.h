#pragma once

#include <optional>
#include <sstream>
#include <string>

std::optional<int> string_to_int(const std::string& str) {
    int i{};
    std::stringstream ss(str);
    ss >> i;
    if (ss.fail()) {
        return std::nullopt;
    }
    return std::make_optional(i);
}
