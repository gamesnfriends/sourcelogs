#include "MainView.h"
#include "../src/StringToInt.h"

static constexpr size_t rows_per_page{25};

void MainView::asyncHandleHttpRequest(
    const drogon::HttpRequestPtr& req,
    std::function<void(const drogon::HttpResponsePtr&)>&& callback) {

    auto params = req->getParameters();

    auto client_ptr = drogon::app().getDbClient("mysql");

    auto page_num = std::max(0, string_to_int(params["page"]).value_or(0));

    auto ret = client_ptr->execSqlAsyncFuture(
        "select * from chat_log ORDER BY date DESC LIMIT ?, ?",
        rows_per_page * page_num, rows_per_page, 2);

    try {

        auto rows = ret.get();
        drogon::HttpViewData data;
        data.insert("title", "Chatlogs");
        data.insert("page_num", page_num);
        std::vector<std::tuple<std::string, std::string>> vec{};
        for (const auto& row : rows) {
            vec.emplace_back(std::make_tuple<std::string, std::string>(
                row["name"].as<std::string>(),
                row["message"].as<std::string>()));
        }
        data.insert("logs", vec);
        auto resp = drogon::HttpResponse::newHttpViewResponse("Logs.csp", data);
        callback(resp);

    } catch (const drogon::orm::DrogonDbException& e) {

        drogon::HttpViewData data;
        data.insert("title", "Error");
        std::cerr << "error:" << e.base().what() << std::endl;
        data.insert("logs",
                    std::vector<std::tuple<std::string, std::string>>{});
        auto resp = drogon::HttpResponse::newHttpViewResponse("Logs.csp", data);
        callback(resp);
    }
}
