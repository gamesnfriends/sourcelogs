#pragma once
#include <drogon/HttpSimpleController.h>

class MainView : public drogon::HttpSimpleController<MainView> {
public:
    void
    asyncHandleHttpRequest(const drogon::HttpRequestPtr& req,
                           std::function<void(const drogon::HttpResponsePtr&)>&&
                               callback) override;
    PATH_LIST_BEGIN
    // list path definitions here;
    // PATH_ADD("/path","filter1","filter2",HttpMethod1,HttpMethod2...);
    PATH_ADD("/");
    PATH_LIST_END
};
