#!/bin/sh
set -eu

PROJECT_NAME="source-logs"

ROOTDIR="$(dirname -- "$0")"

mkdir -p "$ROOTDIR/build"
cd "$ROOTDIR/build"

cmake ..
make

"./$PROJECT_NAME"
